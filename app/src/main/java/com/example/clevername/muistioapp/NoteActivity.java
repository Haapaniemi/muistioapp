package com.example.clevername.muistioapp;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

public class NoteActivity extends AppCompatActivity {

    /*
    * Aktiviteetti muistiinpanojen lisäämiselle ja muokkaamiselle.
     */

    private EditText et_title;
    private EditText et_content;

    private String noteFileName;
    private Note loadedNote;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note);

        et_title = (EditText) findViewById(R.id.note_et_title);
        et_content = (EditText) findViewById(R.id.note_et_content);

        // Haetaan tallennettu tiedoston nimi (Jota käyttäjä oli painanut
        // MainActivity:ssa)
        noteFileName = getIntent().getStringExtra("NOTE_FILE");

        // Tarkistetaan onko Aktiviteettiin päädytty painamalla jo luotua muistiinpanoa
        // vai ollaanko luomassa uutta muistiinpanoa eli noteFileName = null.
        if(noteFileName != null && !noteFileName.isEmpty()) {
            loadedNote = Utilities.getNoteByName(getApplicationContext(),noteFileName);

            if(loadedNote != null) {
                et_title.setText(loadedNote.getTitle());
                et_content.setText(loadedNote.getContent());

            }
        }
    }

    // ladataan menun xml
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_note_new, menu);
        return true;
    }


    /*
    * Menu baarin tallennus ja poisto nappien liitetty toiminnallisuus
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch(item.getItemId()) {

            case R.id.action_note_save:
                saveNote();
                break;

            case R.id.action_note_delete:
                deleteNote();
                break;

        }
        return true;
    }



    private void saveNote() {
        Note note;

        // Tarkistetaan onko käyttäjä lisännyt textField:hin mitään
        if(et_title.getText().toString().trim().isEmpty() || et_content.getText().toString().trim().isEmpty()) {
            Toast.makeText(this, "Lisää muistiinpanolle otsikko ja/tai sisältöä.", Toast.LENGTH_SHORT).show();
            return;
        }

        /*
        * Tarkistetaan onko loadednote null, jos on noten 'dateTime' haetaan system:ilta
        * , muutoin jos vanhaa editoidaan haetaan se 'loadednote'.getDateTime.
        *
        * Muistiinpanojen tiedosto nimethän oli dateTime + .bin. Siksi tärkeää tarkistaa tässä.
        * */
        if(loadedNote == null) {
            note = new Note(System.currentTimeMillis(),
                    et_title.getText().toString(), et_content.getText().toString());
        } else {
            note = new Note(loadedNote.getDateTime(),
                    et_title.getText().toString(), et_content.getText().toString());
        }

        // Kutsutaan Utilities-luokan saveNote-funktiota ja tarkistetaan tietenkin onnistuiko
        // tallennus vaiko ei.
        if(Utilities.saveNote(this, note)) {
            Toast.makeText(this, "Muistiinpano tallennettu", Toast.LENGTH_SHORT).show();
            finish();
        } else {
            Toast.makeText(this, "Muistiinpanon tallenus epäonnistui", Toast.LENGTH_SHORT).show();
        }

    }

    private void deleteNote() {
        if(loadedNote == null) {
            finish();
        }else {

            // Varmistetaan vielä käyttäjältä, että hän haluaa poistaa muistiinpanon
            AlertDialog.Builder dialog = new AlertDialog.Builder(this)
                    .setTitle("Delete")
                    .setMessage("Olet poistamassa muistiinpanon " + et_title.getText().toString() + ", oletko varma?")
                    .setPositiveButton("Kyllä", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // Muistiinpanon poisto
                            Utilities.deleteNote(getApplicationContext(), loadedNote.getDateTime() + Utilities.FILE_EXTENSION);
                            Toast.makeText(getApplicationContext(), et_title.getText().toString() + " muistiinpano poistettiin", Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    })
                    .setNegativeButton("Ei", null)
                    .setCancelable(false);

            dialog.show();
        }
    }
}
