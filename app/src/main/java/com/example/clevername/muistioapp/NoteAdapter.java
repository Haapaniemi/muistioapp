package com.example.clevername.muistioapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class NoteAdapter extends ArrayAdapter<Note> {


    // Konstruktori adapterille
    public NoteAdapter(Context context, int resource, ArrayList<Note> objects) {
        super(context, resource, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //return super.getView(position, convertView, parent);

        // tarkistetaan, että annettu view ei ole null, jos on
        // inflantoidaan haluttu View --> layout.item_note
        if(convertView == null) {
            convertView = LayoutInflater.from(getContext())
                    .inflate(R.layout.item_note, null);
        }

        /*
        * Haetaan position:in avulla muistiinpano
         */
        Note note = getItem(position);

        /*
        * Tarkistetaan, että muistiinpano ei ole null, ja jos ei ole, niin
        * asetetaan xml-elementtien arvot muistiinpanon arvoilla vastaavaksi.
         */
        if(note != null) {
            TextView title = (TextView) convertView.findViewById(R.id.list_note_title);
            TextView date = (TextView) convertView.findViewById(R.id.list_note_date);
            TextView content = (TextView) convertView.findViewById(R.id.list_note_content);

            title.setText(note.getTitle());
            date.setText(note.getDateTimeFormatted(getContext()));

            // Ei näytetä koko merkkijonoa, jos yli 50 kirjainta pitkä
            if(note.getContent().length() > 50) {
                content.setText(note.getContent().substring(0,50));
            } else {
                content.setText(note.getContent());
            }
        }

        // Lopuksi palautetaan luotu näkymä
        return convertView;
    }
}
