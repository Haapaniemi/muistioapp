package com.example.clevername.muistioapp;

import android.content.Context;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/*
* Yksinkertainen luokka Muistiinpanoille.
 */

public class Note implements Serializable {

    private long DateTime;
    private String Title;
    private String Content;


    public Note(long dateTime, String title, String content) {
        DateTime = dateTime;
        Title = title;
        Content = content;
    }

    public long getDateTime() {
        return DateTime;
    }

    public void setDateTime(long dateTime) {
        DateTime = dateTime;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getContent() {
        return Content;
    }

    public void setContent(String content) {
        Content = content;
    }

    public String getDateTimeFormatted(Context context) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss",
                context.getResources().getConfiguration().locale);
        dateFormat.setTimeZone(TimeZone.getDefault());
        return dateFormat.format(new Date(DateTime));
    }
}
