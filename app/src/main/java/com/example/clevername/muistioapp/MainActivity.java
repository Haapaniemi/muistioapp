package com.example.clevername.muistioapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private ListView notesListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        notesListView = findViewById(R.id.notesView);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
     }

     @Override
     public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.action_main_new_note:
                //aloita NoteActivity 'newNote'
                startActivity(new Intent(this, NoteActivity.class));
                break;
        }
        return true;
     }

     @Override
    protected void onResume() {
        super.onResume();
        notesListView.setAdapter(null);

         ArrayList<Note> notes = Utilities.getAllSavedNotes(this);

         /*
         * Tarkistetaan onko muistissa tallennettuja muistiinpanoja ja ilmoitetaan käyttäjälle
         * jos ei ole.
          */
         if(notes == null || notes.size() == 0) {
             Toast.makeText(this, "Ei ole tallennettuja muistiinpanoja", Toast.LENGTH_SHORT).show();
             return;
         } else {
            /*
            * Asetaan muistiinpanojen listanäkymälle adapteri
             */
             NoteAdapter na = new NoteAdapter(this, R.layout.item_note, notes);
             notesListView.setAdapter(na);

             // Haetaan käyttäjän painama muistiinpano
             notesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                 /*
                 * Kun käyttäjä painaa mainActivity:ssa muistiinpanoa, otetaan sen muistiinpanon tiedostonimi
                 * talteen ja luodaan sillä uusi Intent, johon saadaan tallennettua tiedostonnimi. Aloitetaan
                 * NoteActivity-aktiviteetti luodulla Intent:lla.
                  */
                 @Override
                 public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                     String filename = ((Note) notesListView.getItemAtPosition(position)).getDateTime()
                             + Utilities.FILE_EXTENSION;
                     Intent viewNoteIntent = new Intent(getApplicationContext(),NoteActivity.class);
                     viewNoteIntent.putExtra("NOTE_FILE", filename);
                     startActivity(viewNoteIntent);
                 }
             });

         }

     }

}
